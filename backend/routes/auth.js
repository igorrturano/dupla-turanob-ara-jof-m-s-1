const express = require('express');
const router = express.Router();
const Admin = require('./../models/admin')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')


router.post('/', async (req, res, next) => {
    try {
    let {login, senha} = req.body
    let admin = await Admin.findOne({login: login})
    if (admin) {
        let resultPass = await bcrypt.compare(senha, admin.senha)
        if (!resultPass) {
            res.status(403).json({"Erro" : "Senha incorreta"})
        }else{
           const payload = {
               admin: {
                   login: admin.login,
                   senha: admin.senha
               }
           }
           jwt.sign(payload, config.get('jwtSecret'), {expiresIn: '5 days'}, 
           (error, token) => {
              if(error) throw error
            payload.token = token
            res.json(payload)
           })
        }
    }
    } catch (error) {
        console.error(error.message)
        res.status(500).json({"Error":"Server Error"})
    }
})

module.exports = router;