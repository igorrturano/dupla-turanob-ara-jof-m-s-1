const express = require('express');
const router = express.Router()
const Aluno = require('../models/alunos')
const auth = require('./../middleware/auth')

router.get('/', auth, async (req, res, next) => {
    try {
        let aluno = await Aluno.find({})
        res.json(aluno)
    } catch (error) {
        console.error(error.message)
        res.status(500).json({"Error" : "Server Error"})
    }
})

router.post('/', async (req, res, next) => {
    try {
        let { nome, idade, turma, notaFinal } = req.body
        let aluno = new Aluno({ nome, idade, turma, notaFinal })
        await aluno.save()
        res.status(200).json(aluno)
    } catch (error) {
        console.error(error.message)
        res.send(500).send({"Error" : "Server Error"})
    }
})

router.patch('/:alunoId', async (req, res, next) => {
    try {
        let aluno = req.body
        let id = req.params['alunoId']
        const alunoEditado =  await Aluno.findByIdAndUpdate(id, {$set: aluno}, {new: true})
        res.json(alunoEditado)
    } catch (error) {
        console.error(error.message)
        res.send(500).json({"Error" : "Server Error"})
    }
})

router.delete('/:alunoId', async (req, res, next) => {
    try {
        let id = req.params['alunoId']
        let alunoDeletado = await Aluno.findByIdAndDelete(id)
        res.json(alunoDeletado)
    } catch (error) {
        console.error(error.message)
        res.send(500).json({"Error" : "Server Error"})
    }
})

module.exports = router;