const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const Admin = require('./../models/admin')


router.get('/', async (req, res, next) => {
    try {
        let admin = await Admin.find({})
        res.json(admin)
    } catch (error) {
        console.error(error.message)
        res.status(500).send({"Error":"Server Error"})
    }
})

router.get('/:adminId', async (req, res, next) => {
    try {
        let admin = await Admin.findById(req.params.adminId)
        res.json(admin)
    } catch (error) {
        console.error(error.message)
        res.status(500).json({"Error":"Server Error"})
    }
})

router.post('/', async (req, res, next) => {
   try {
    let { login, senha } = req.body
    let admin = new Admin({login, senha})
    const cryptPass = await bcrypt.genSalt(10)
    admin.senha = await bcrypt.hash(senha, cryptPass )

    await admin.save()

    if(admin.id){
        res.json(admin)
    }
    
   } catch (error) {
       console.error(error.message)
       res.status(500).json({"Error" : "Server Error"})
   }
})

module.exports = router;