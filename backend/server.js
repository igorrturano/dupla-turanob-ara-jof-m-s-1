const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const PORT = 3004 || process.env.PORT
const connectDB = require('./config/db')

// Middleware
app.use(express.json())
app.use(bodyParser.urlencoded({extended : true}))

// MongoDB
connectDB();

// Rotas
app.use('/alunos', require('./routes/alunos'))
app.use('/admin', require('./routes/admin'))
app.use('/auth', require('./routes/auth'))

app.listen(PORT, console.log(`Conectado à porta ${PORT}`))