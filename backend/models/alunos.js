const mongoose = require('mongoose');
const Aluno = new mongoose.Schema({
    nome: {
        type: String,
        required:true
    },
    idade: {
        type: Number,
        required:true
    },
    turma: {
        type: Number,
        required: true
    },
    notaFinal: {
        type: Number,
        required: true
    }     
})

module.exports = mongoose.model('aluno', Aluno)