const jwt = require('jsonwebtoken')
const config = require('config')

module.exports = function (req, res, next) {
  try {
    const token = req.header('x-auth-token')
    if(!token){
        return res.status(401).json({"Error":"Unauthorized"})
    }
    jwt.verify(token, config.get('jwtSecret'), (error, decoded) => {
      if (error) {
         return res.status(401).json({"Error":"Invalid Token"})
      }
      next()
    })
  } catch (error) {
      console.error("Error in authentication middleware")
      res.status(500).json({"Error": "Server Error"})
  }
}